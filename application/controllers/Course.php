<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {
	
	public function create() {
		$this->load->view('course/create');
    }
	
	public function save() {
		$this->load->model('courses');
		$course_id = $this->courses->insert_course($_POST, $_FILES);
		$this->load->view('course/success', ['curso'=>$course_id]);
	}
	
	public function success() {
		$this->load->view('course/success');
	}

	public function seeCourse($course_id) {
		$this->load->model('courses');
		$course = $this->courses->getCourse($course_id);
		if($course){
			$this->load->view('course/course', $course);
		}else
			echo 'Curso inexistente';
	}

	public function delete($course_id) {
		$this->load->model('courses');
		echo 'Curso deletado com sucesso';
		$course = $this->courses->deleteCourse($course_id);
	}
	
	public function edit($course_id) {
		$this->load->model('courses');
		$course = $this->courses->getCourse($course_id);
		if(!$course){
			echo 'Curso inexistente';
			die;
		}
		$this->load->view('course/create', ['course' => $course["course"]]);
	}
	
	public function update() {
		$this->load->model('courses');
		$course = $this->courses->updateCourse($_POST);
		header('Location: /curso/' . $_POST["course_id"]);
	}
    
}
