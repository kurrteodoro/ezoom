<?php

class Courses extends CI_Model {

    public $name;
    public $description;

    public function get_courses()
    {
            $query = $this->db->get('courses');
            return $query->result();
    }

    public function insert_course($request, $files)
    {

        $id = time();
        $this->name = $request['name'];
        $this->description = $request['description'];
        $this->pic = $id . $files["pic"]["name"][0];
        $this->db->insert('courses', $this);

        $course_id = $this->db
        ->order_by('id', 'DESC')
        ->get_where('courses', [
            'name' => $this->name,
            'description' => $this->description,
            'pic' => $this->pic,
        ])
        ->row()
        ->id;

        if(!is_dir(__DIR__ . '/../../storage/img_courses'))
            mkdir(__DIR__ . '/../../storage/img_courses');
            
        foreach($files["pic"]["tmp_name"] as $key => $file) {
            move_uploaded_file($file, __DIR__ . '/../../storage/img_courses/' . $id . $files["pic"]["name"][$key]);
            if($key != 0){
                $this->db->insert('img_courses', [
                    'course_id' => $course_id,
                    'image' => $id . $files["pic"]["name"][$key]
                ]);
            }
        }

        return $course_id;
                
    }

    public function updateCourse($request)
    {
            $this->db->update('courses', 
                [
                    'name' => $request["name"],
                    'description' => $request["description"]
                ], 
                [
                    'id' => $request["course_id"],
                ]
            );
    }

    public function getCourse($course_id) {
        
        $course = $this->db->get_where('courses', [
            'id' => $course_id
        ])->row();

        if(!$course)
            return null;

        $images = $this->db->get_where('img_courses', [
            'course_id' => $course_id
        ])->result();

        return ['course' => $course, 'images' => $images];
    }
    
    public function getCourses() {
        return $course = $this->db->get('courses')->result();
    }

    public function deleteCourse($course_id) {
        $this->db->delete('courses', [
            'id' => $course_id
        ]);
    }

}