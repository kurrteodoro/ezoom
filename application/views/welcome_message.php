<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet/less" type="text/css" href="/assets/less/styles.less" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400&display=swap" rel="stylesheet">
    <title>Home</title>
</head>
<body>

<header class="bk-home">
    <div class="container">
        <div class="flex">
            <img src="/assets/images/logo.png" class="logo" alt="logo">
            <img src="/assets/images/menu.png" id="menu" class="menu">
        </div>
        <div class="header-content">
            <h1>Cursos e treinamentos</h1>
            <p>Selecione o curso ou treinamento que deseja</p>
        </div>
    </div>
    <nav class="menu-right">
        <div class="container">
            <img src="/assets/images/close.png" id="close" class="close" alt="">
        </div>
        <ul>
            <li>
                <a href="/course/create">Criar curso</a>
            </li>
        </ul>
    </nav>
</header>
    
    <div class="container">
        <div class="box">
            <div class="container">
                <ul class="none">
                    <?php foreach($courses as $course): ?>
                    <a href="curso/<?php echo $course->id ?>">
                        <li class="flex item-list-course">
                            <p class="none course-name"><?php echo $course->name; ?></p>
                            <span class="flex">
                                <div style="background-image: url('/storage/img_courses/<?php echo $course->pic; ?>');"></div>
                            </span>
                        </li>
                    </a>
                    <?php endforeach; ?>
                    <?php if(!$courses): ?>
                        <li>
                            <p class="none course-name">Sem cursos</p>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/less"></script>
    <script src="/assets/js/nav.js"></script>

</body>
</html>