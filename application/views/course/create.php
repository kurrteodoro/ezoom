<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet/less" type="text/css" href="/assets/less/styles.less" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400&display=swap" rel="stylesheet">
    <title><?php echo isset($course) ? 'Editar' : 'Cadastrar' ?> curso</title>
</head>
<body>

<header class="bk-create">
    <div class="container">
        <div class="flex">
            <img src="/assets/images/logo.png" class="logo" alt="logo">
            <img src="/assets/images/menu.png" id="menu" class="menu">
        </div>
        <div class="header-content">
            <h1><?php echo isset($course) ? 'Edite' : 'Crie' ?>  um curso ou treinamento</h1>
        </div>
    </div>
    <nav class="menu-right">
        <div class="container">
            <img src="/assets/images/close.png" id="close" class="close" alt="">
        </div>
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
        </ul>
    </nav>
</header>
    
    <div class="container">
        <div class="box">
            <div class="container">
                
            <form action="/<?php echo isset($course) ? 'editar' : 'criar' ?>/salvar" enctype="multipart/form-data" method="POST">
                
                <label for="nome">Nome do curso</label>
                <input <?php echo isset($course) ? 'value="'.$course->name.'"' : '' ?> required type="text" name="name" id="nome" class="input" placeholder="Curso de montagem">                
                
                <label for="descricao">Descrição do curso</label>
                <input <?php echo isset($course) ? 'value="'.$course->description.'"' : '' ?> required type="text" name="description" id="descricao" class="input" placeholder="Curso legal de montagem">

                <?php if(!isset($course)): ?>

                <span>A primeira foto é obrigatória, servirá como capa</span>

                <div class="flex wrap mt-20">
                    <div class="list-pic flex wrap">
                        <div class="main_pic pic">
                            <input required name="pic[]" type="file" class="hidden">
                        </div>
                    </div>
                    <div class="plus"></div>
                </div>

                <?php else: ?>
                    <input type="hidden" value="<?php echo $course->id ?>" name="course_id">
                <?php endif; ?>

                <button type="submit" class="btn"><?php echo isset($course) ? 'Editar' : 'Criar' ?></button>

            </form>

            </div>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/less"></script>
    <script src="/assets/js/nav.js"></script>
    <script src="/assets/js/course_create.js"></script>

</body>
</html>