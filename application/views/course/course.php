<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet/less" type="text/css" href="/assets/less/styles.less" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400&display=swap" rel="stylesheet">
    <title><?php echo $course->name ?></title>
</head>
<body>

<header class="bk-home">
    <div class="container">
        <div class="flex">
            <img src="/assets/images/logo.png" class="logo" alt="logo">
            <img src="/assets/images/menu.png" id="menu" class="menu">
        </div>
        <div class="header-content">
            <h1><?php echo $course->name ?></h1>
        </div>
    </div>
    <nav class="menu-right">
        <div class="container">
            <img src="/assets/images/close.png" id="close" class="close" alt="">
        </div>
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/course/create">Criar curso</a>
            </li>
        </ul>
    </nav>
</header>
    
    <div class="container">
        <div class="box">
            <div class="container">
                <p><?php echo $course->description ?></p>
                <div class="flex images-course w100 wrap">
                    <img src="/storage/img_courses/<?php echo $course->pic ?>" alt="">
                    <?php foreach($images as $image): ?>
                        <img src="/storage/img_courses/<?php echo $image->image ?>" alt="">
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <form action="/curso/apagar/<?php echo $course->id; ?>">
            <button class="btn danger">Apagar curso</button>
        </form>
        <form action="/curso/editar/<?php echo $course->id; ?>">
            <button class="btn info">Apagar editar</button>
        </form>
    </div>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/less"></script>
    <script src="/assets/js/nav.js"></script>

</body>
</html>