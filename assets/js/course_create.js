$( document ).ready(function() {
    
    setClickPic();

    $(".plus").on('click', function(){
        
        var html = `
        <div class="main_pic pic">
            <input required name="pic[]" type="file" class="hidden">
        </div>`;
        
        $('.list-pic').append(html);
        setClickPic();
        $('.list-pic').children().last()[0].click();
    });

    function setClickPic() {
        $(".pic").off('click').on('click', function() {
            $(this).children().on('change', function() {
                var input = $(this)[0];
                if(input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                    $(input).parent()
                    .css('background-image', `url('${e.target.result}')`);
                };
                reader.readAsDataURL(input.files[0]);
                }
                else {
                    var img = input.value;
                    $(input).next().attr('src',img);
                }
            });
            $(this).children()[0].click()
        });
    }



});